const userRepository = require("../Repository/UserRepository");

//create new user
exports.create = function (req , res) {
    userRepository.create(req.body).then((todo)=>{
        res.send(todo);
    }).catch(err =>{
        res.status(err.status || 500).json({message: err.message, error: err});
    })
}
