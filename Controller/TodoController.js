const todoRepository = require("../Repository/TodoRepository");

//create new UserTodo
exports.create = function (req , res) {
    todoRepository.create(req).then((todo)=>{
        res.send(todo);
    }).catch(err =>{
        res.status(err.status || 500).json({message: err.message, error: err});
    })
}

//Get todoItem by id
exports.getById = function (req , res) {
    todoRepository.getById(req.params.id).then((todoData)=>{
        res.send(todoData);
    }).catch(err =>{
        res.status(err.status || 500).json({message: err.message, error: err});
    })
}

//update specific UserTodo
exports.update = function (req , res) {
    todoRepository.update(req.params.id , req.body).then((todoAfterUpdate) =>{
        res.send(todoAfterUpdate);
    }).catch(err =>{
        res.status(err.status || 500).json({message: err.message, error: err});
    })
}

//delete specific UserTodo
exports.delete = function (req , res) {
    todoRepository.delete(req.params.id).then((response)=>{
        res.status(200).json({message:"Deleted successfully"});
    }).catch(err => {
        res.status(err.status || 500).json({message: err.message, error: err});
    })
}

//Get userTodo list
exports.getUserTodo = function (req , res) {
    todoRepository.getUserTodoList(req).then((todoList)=>{
        res.send(todoList);
    }).catch(err =>{
        res.status(err.status || 500).json({message: err.message, error: err});
    })
}
