const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const userModel = require('../Model/User');
const jwtStrategy = require('passport-jwt').Strategy;
const extractJwt = require('passport-jwt').ExtractJwt;

passport.use(
    'login',
    new localStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback : true
        },
        async (req, email, password, done) => {
            try {
                const user = await userModel.findOne({ email });
                if (!user) {
                    return done(null, false, { message: 'user not found' });
                }

                if(user.password !== password)
                {
                    return done(null, false, { message: 'Wrong Password' });
                }

                return done(null, user, { message: 'Logged in Successfully' });
            } catch (error) {
                return done(error);
            }
        }
    )
);

//Verifying the JWT
passport.use(
    new jwtStrategy(
        {
            secretOrKey: 'TOP_SECRET',
            jwtFromRequest: extractJwt.fromAuthHeaderAsBearerToken('jwt')
        },
        async (user, done) => {
            try {
                return done(null, user);
            } catch (error) {
                console.log(error);
                done(error);
            }
        }
    )
);
