const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

var todoSchema = new Schema(
    {
        user_id :{
            type: Schema.ObjectId,
            ref: 'users',
            immutable: true  //lock the field from being updated
        },
        description :{
            type:String,
            required: [true, 'Description is required!']
        },
        completed :{
            type:Boolean,
            defaults: 0
        },
        createdAt :{
            type: Date,
            defaults: Date.now(),
            immutable: true   //lock the field from being updated
        }
    })

module.exports = Mongoose.model("todos",todoSchema);