const Mongoose = require("mongoose");
const Crypto = require('crypto');
const Schema = Mongoose.Schema;

var userSchema = new Schema({
        first_name: {
            type: String,
            required: [true, 'First name is required!']
        },
        last_name: {
            type: String,
            required: [true, 'Last name is required!']
        },
        email: {
            type: String,
            required: [true, 'Email is required!'],
            unique: true
        },
        password : {
            type: String,
            set: encrypt,
            get: decrypt,
            required: [true, 'Password is required!'],
        },
    }
    ,
    {
        toJSON: {getters: true, setters: true}
    }
)

//indexing by FName , LName , email
userSchema.index({
    first_name:'text',
    last_name:'text',
    email:'text'
});

function encrypt(text) {
    try {
        var cipher = Crypto.createCipher('aes-256-cbc', 'password');
        var encrypted = cipher.update(text, 'utf8', 'hex');
        encrypted += cipher.final('hex');
        return encrypted;
    }
    catch (e) {
        console.log("Failed");
        throw (e)
    }

}

function decrypt(text) {
    try {
        if (text === null || typeof text === 'undefined') { return text; }
        var decipher = Crypto.createDecipher('aes-256-cbc', 'password');
        var dec = decipher.update(text, 'hex', 'utf8');
        dec += decipher.final('utf8');
        return dec;
    }
    catch (e) {
        console.log("Failed");
        throw (e)
    }
}

module.exports = Mongoose.model("users",userSchema);