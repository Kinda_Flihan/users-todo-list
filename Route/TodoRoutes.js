const express = require("express");
const router = express.Router();
const todoController = require("../Controller/TodoController");

router.post('/',todoController.create);
router.get('/userTodo',todoController.getUserTodo);
router.get('/:id',todoController.getById);
router.put('/:id',todoController.update);
router.delete('/:id',todoController.delete);

module.exports = router;