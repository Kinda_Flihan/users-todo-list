const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
require('../Authentication/JwtStrategy');


router.post(
    '/',
    async (req, res, next) => {
        passport.authenticate(
            'login',
            async (err, user, info) => {
                try {
                    if (err || !user) {

                        const error = new Error(info.message);

                        return next(error);
                    }

                    req.login(
                        user,
                        { session: false },
                        async (error) => {
                            if (error) return next(error);

                            const token = jwt.sign({ user: user }, 'TOP_SECRET',{ expiresIn: '8h' });

                            return res.json({ user , token});
                        }
                    );
                } catch (error) {
                    return next(error);
                }
            }
        )(req, res, next);
    }
);


module.exports = router;
