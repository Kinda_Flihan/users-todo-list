const express = require('express');
const router = express.Router();
const todoRoutes = require('./TodoRoutes');
const userRoutes = require('./UserRoutes');
const loginRoute = require('./Login');


router.use('/todo',todoRoutes);
router.use('/user',userRoutes);
router.use('/login',loginRoute);


module.exports=router;