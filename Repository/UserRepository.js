const user = require("../Model/User");
const jwt = require('jsonwebtoken');
const accessTokenSecret = 'TOP_SECRET';

//create new user
exports.create = async function (userInformation) {
    return new Promise((resolve, reject) => {

        user.create(userInformation, (err, response) => {
            if (err) {
                reject(err);
            } else {
                resolve(response);
            }
        })
    })
}

exports.GetUserInfo = async function (req) {
    return new Promise((resolve, reject) => {
        if(req.headers && req.headers.authorization){
            var authorization = req.headers.authorization.split(' ')[1],
                decoded;
            try {
                decoded = jwt.verify(authorization, accessTokenSecret);
            } catch (e) {
                var error = {
                    'status': 401,
                    'message': "unauthorized"
                }
                reject(error);
            }

            var userId = decoded.user._id;
            // Fetch the user by id
            user.findOne({_id: userId} , (err , response)=>{
                if(err) {
                    reject(err);
                }
                if(response){
                    resolve(response);
                }
                else {
                    var error = {
                        'status': 404,
                        'message': "Not found"
                    }
                    reject(error);
                }
            })
        }
        else {
            var error = {
                'status': 401,
                'message': "unauthorized"
            }
            reject(error);
        }
    })

}