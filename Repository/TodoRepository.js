const todo = require("../Model/Todo");
const userRepository = require('./UserRepository');

//create todoItem
exports.create = async function (req) {
    return new Promise((resolve, reject) => {
        const requestInformation = req.body;
        userRepository.GetUserInfo(req).then((user) => {
            requestInformation.createdAt = Date.now();
            requestInformation.completed = false;
            requestInformation.user_id = user._id;
            todo.create(requestInformation, (err, response) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(response);
                }
            })
        }).catch(err =>{
            reject(err);
        })
    })
}

//get specific todoItem by id
exports.getById = async function (todoId) {
    return new Promise((resolve, reject) => {
        todo.findById(todoId, (err, response) => {
            if (err) {
                reject(err);
            }
            if (response) {
                resolve(response);
            } else {
                var error = {
                    'status': 404,
                    'message': "Not found"
                }
                reject(error);
            }
        })
    })
}

//update todoItem
exports.update = async function (todoId, newData) {
    return new Promise((resolve, reject) => {
        todo.findByIdAndUpdate(todoId, newData, {new: true}, (err, response) => {
            if (err) {
                reject(err);
            }
            if (response) {
                resolve(response);
            } else {
                var error = {
                    'status': 404,
                    'message': "The todo did not update"
                }
                reject(error);
            }
        })
    })
}

//delete specific todoItem
exports.delete = async function (todoId) {
    return new Promise((resolve, reject) => {
        todo.findByIdAndDelete(todoId, (err, response) => {
            if (err) {
                reject(err);
            }
            if (response) {
                resolve(response);
            } else {
                var error = {
                    'status': 404,
                    'message': "The todo did not delete"
                }
                reject(error);
            }
        })
    })
}

// Get userTodo list
exports.getUserTodoList = async function(req){
    return new Promise((resolve, reject) => {
        userRepository.GetUserInfo(req).then((user)=>{
            todo.find({ user_id: user._id} , (err , response)=>{
                if (err) {
                    reject(err);
                }
                if (response) {
                    resolve(response);
                } else {
                    var error = {
                        'status': 404,
                        'message': "Not found"
                    }
                    reject(error);
                }
            })
        }).catch(err =>{
            reject(err);
        })
    })
}