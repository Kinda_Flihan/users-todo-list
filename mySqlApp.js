const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const mysql = require('mysql');
const https = require('https');
const bodyParser = require('body-parser');


const app2 = express();

app2.use(bodyParser.json());
app2.use(bodyParser.urlencoded({ extended: true }));
app2.use(express.static(path.join(__dirname, 'public')));

//Create table if not exist
const sqlCreateTables = require('./MySql/SqlTables');

//prefix api for all routes
const mysqlIndexRouter = require('./MySql/Route');
app2.use('/api', mysqlIndexRouter);


app2.listen(3001,function (){
    console.log("Server is running on port 3001.");
});