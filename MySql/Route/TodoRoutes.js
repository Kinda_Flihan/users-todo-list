const express = require("express");
const router = express.Router();
const todoController = require('../Controller/TodoController');


router.post('/', todoController.create);
router.get('/:id', todoController.getById);

module.exports = router;