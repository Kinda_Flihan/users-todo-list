'use strict';
const mysql = require('mysql');
//local mysql db connection
var sqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database:'todo'
})

sqlConnection.connect(function(err) {
    if (err) throw err;
    console.log("MYSQL CONNECTED!");
});

module.exports = sqlConnection;