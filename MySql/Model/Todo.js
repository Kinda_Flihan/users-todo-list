var dbConnection = require('../Configuration/sqlConfiguration');

//Create todo object
var todo = function (todo){
    this.description = todo.description;
    this.completed = todo.completed;
}

todo.create = function (newTodo , result){
    dbConnection.query("INSERT INTO todos set ?", newTodo, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    })
}

todo.getById = function (id , result){
    dbConnection.query("SELECT * from todos WHERE id=? ", id, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    })
}

module.exports= todo;