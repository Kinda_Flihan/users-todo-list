const todo = require('../Model/Todo');

//create new UserTodo
exports.create = function (req, res) {
//handles null error
    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        res.status(400).send({error: true, message: 'Please provide all required field'});
    } else {
        const new_todo = new todo(req.body);
        new_todo.completed = false;
        todo.create(new_todo, function (err, todo) {
            if (err)
                res.send(err);
            res.json({message: "Added successfully!", data: todo});
        });
    }
};

//Get todoItem by id
exports.getById = function (req, res) {

    todo.getById(req.params.id, function (err, todo) {
        if (err)
            res.send(err);
        res.send(todo);
    });
};