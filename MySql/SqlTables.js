const dbConnection = require('./Configuration/sqlConfiguration');

let createTodos = `create table if not exists todos(
                          id int primary key auto_increment,
                          description varchar(255)not null,
                          completed tinyint(1) not null default 0
                      )`;

dbConnection.query(createTodos, function(err, results, fields) {
    if (err) {
        console.log(err.message);
    }
});