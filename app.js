const express = require("express");
const createError = require('http-errors');
const bodyParser = require("body-parser");
const request = require("request");
const https = require('https');
const ejs = require("ejs");
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();

const app = express();

// Connect to mongo database
mongoose.connect('mongodb://127.0.0.1/UsersTodoList', { useNewUrlParser: true, useUnifiedTopology: true })

// Mongodb default connection
var dbConnection = mongoose.connection;
dbConnection.on('error', console.error.bind(console, 'MongoDB connection error:'));
dbConnection.once("open", function callback() {
    console.log("MONGODB CONNECTED");
});



app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));


//prefix api for all routes
const indexRouter = require('./Route');
app.use('/api',indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send(err.message);
});

app.listen(3000,function (){
    console.log("Server is running on port 3000.")
});