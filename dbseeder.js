const faker = require("faker");
const Seeder = require("mysql-db-seed").Seeder;

// Generate a new Seeder instance
const seed = new Seeder(
    10,
    "localhost",
    "root",
    "",
    "todo"
);

(async () => {
    await seed.seed(
        30,
        "todos",
        {
            description: faker.lorem.words(50),
            completed: faker.random.boolean()
        }
    )
    seed.exit();
    process.exit();
})();